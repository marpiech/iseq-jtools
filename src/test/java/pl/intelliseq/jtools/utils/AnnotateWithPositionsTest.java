package pl.intelliseq.jtools.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.intelliseq.jtools.JtoolsApp;
import pl.intelliseq.jtools.tools.AnnotateWithPositions;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JtoolsApp.class)
public class AnnotateWithPositionsTest {

	@Value("${input.vcf}")
    private String vcfFile;
	
	@Value("${input.pos}")
	private String posFile;
	
	@Autowired
	private DevNullOutputStream devNullOutputStream;
	
	@Autowired
	private AnnotateWithPositions annotateWithPositions;
	
	@Test
	public void AnnotateTest() {

		annotateWithPositions.annotate(vcfFile, posFile, devNullOutputStream);

	}
}