package pl.intelliseq.jtools.tsv;

import java.io.File;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.intelliseq.jtools.JtoolsApp;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JtoolsApp.class)
public class PosFileTest {

	@Value("${input.vcf}")
    private String vcfFile;
	
	@Test
	public void AnnotateTest() {

		PosFile tool = new PosFile(new File("largefiles/ESP_AF.pos"));

	}
}