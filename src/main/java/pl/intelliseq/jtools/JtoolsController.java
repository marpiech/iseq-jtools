package pl.intelliseq.jtools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

import pl.intelliseq.jtools.tools.AnnotateWithPositions;
 
@Controller
public class JtoolsController implements CommandLineRunner {
 
    private Logger log = Logger.getLogger(JtoolsController.class);

	@Value("${input.vcf}")
    private String vcfFile;
	
	@Value("${input.pos}")
	private String posFile;

	@Value("#{systemProperties.getProperty('output.vcf') ?: null}")
	private String output;
	
    @Autowired
	AnnotateWithPositions annotateWithPositions;
    
    @Override
    public void run(String... args) throws Exception {
    	
    	if (args.length > 0) {

    		if(args[0].equals("annopos")) {
    			
    			log.info("running annopos");
    			annotateWithPositions.annotate(vcfFile, posFile, this.getOutputStream(output));
    			
    		}
    		
    	}
   	
    }
    
    /**
     * generates FileOutputStream if path is given or System.out if null is submitted
     * @param output Path to output file or null if stdout
     * @return OutputStream
     */
    private OutputStream getOutputStream(String output) {
    	OutputStream out;
    	if (output != null) {
    		try {
				out = new FileOutputStream(new File(output));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
    	} else {
    		out = System.out;
    	}
    	return out;
    }
    
}