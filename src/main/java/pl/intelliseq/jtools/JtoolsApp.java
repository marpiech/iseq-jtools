package pl.intelliseq.jtools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import pl.intelliseq.jtools.tools.AnnotateWithPositions;
import pl.intelliseq.jtools.utils.DevNullOutputStream;
 
@ComponentScan
@EnableAutoConfiguration
public class JtoolsApp {
 
	@Bean
    DevNullOutputStream getDevNullOutputStream() {
        DevNullOutputStream stream = new DevNullOutputStream();
        return stream;
    }
	
	@Bean
	AnnotateWithPositions getAnnotateWithPositions() {
		AnnotateWithPositions tool = new AnnotateWithPositions();
		return tool;
	}
	
    public static void main(String[] args) {
        SpringApplication.run(JtoolsApp.class, args);
    }

}