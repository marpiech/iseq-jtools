package pl.intelliseq.jtools.tools;

import htsjdk.samtools.util.CloseableIterator;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.variantcontext.writer.VariantContextWriter;
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder;
import htsjdk.variant.vcf.VCFFileReader;
import htsjdk.variant.vcf.VCFHeader;
import htsjdk.variant.vcf.VCFHeaderLineType;
import htsjdk.variant.vcf.VCFInfoHeaderLine;

import java.io.File;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import pl.intelliseq.jtools.tsv.PosFile;

public class AnnotateWithPositions {

	private static Logger log = Logger.getLogger(AnnotateWithPositions.class);
	
	@Value("${info.id}")
    private String infoId;
	
	@Value("${info.description}")
    private String infoDescription;
	
	@Value("${info.type}")
    private String infoType;
	
	VCFFileReader reader;
	
	PosFile posFile;
	
	public void annotate(String vcfFilePath, String posFilePath, OutputStream out) {
		
		reader = this.getVcfReader(vcfFilePath);
		posFile = this.getPosFile(posFilePath);
		
		final VCFHeader header = reader.getFileHeader();
		final VariantContextWriter writer = this.getVcfWriter(header, out);
				
		header.addMetaDataLine(
				new VCFInfoHeaderLine(
						infoId,
						1,
						VCFHeaderLineType.valueOf(infoType),
						infoDescription)
				);
		
		writer.writeHeader(header);
		final CloseableIterator<VariantContext> iterator = reader.iterator();

	    while (iterator.hasNext()) {
		    VariantContext variantContext = iterator.next();
		    posFile.annotateVariantContext(variantContext, infoId);
            writer.add(variantContext);
	    }
	    
	    reader.close();

	}
	
	private VCFFileReader getVcfReader(String vcfFilePath) {
		
		File vcfFile = new File(vcfFilePath);
		return new VCFFileReader(vcfFile, false);
		
	}
	
	private PosFile getPosFile(String posFilePath) {
		
		PosFile posFile = new PosFile(new File(posFilePath));
		return posFile;	
		
	}
	
	private VariantContextWriter getVcfWriter(VCFHeader header, OutputStream out) {
		
		VariantContextWriterBuilder vcfBuilder = new VariantContextWriterBuilder()
			.setCreateMD5(false)
			.setOutputStream(out)
			.setReferenceDictionary(null)
			.clearOptions();
		return vcfBuilder.build();
		
	}
	
}

