package pl.intelliseq.jtools.tsv;

import htsjdk.variant.variantcontext.CommonInfo;
import htsjdk.variant.variantcontext.VariantContext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class PosFile {

	Map <String, String> positionVariantToValue = new TreeMap <String, String> ();
	
	public PosFile (File file) {
		
		this.read(file);
		
	}

	public void annotateVariantContext(VariantContext variantContext, String id) {
		
		String key = variantContext.getChr() + "_" +
				variantContext.getStart() + "_" +
				variantContext.getReference().getBaseString() + "_" +
				variantContext.getAltAlleleWithHighestAlleleCount();
		
		String value = positionVariantToValue.get(key);
		if (value != null) {
			
			variantContext.getCommonInfo().putAttribute(id, value);
			
		}
		
	}
	
	private void read(File file) {
		
		BufferedReader reader = null;

		try {
		    reader = new BufferedReader(new FileReader(file));

		    String line;
		    while ((line = reader.readLine()) != null) {
		        
		    	String[] lineElements = line.split("\t");
		    	String positionVariant = lineElements[0] + "_" + lineElements[1] + "_" + lineElements[2] + "_" + lineElements[3];
		    	String value = lineElements[4];
		    	positionVariantToValue.put(positionVariant, value);
		    	
		    }

		} catch (IOException e) {
		    e.printStackTrace();
		} finally {
		    try {
		        reader.close();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		}
	}

	private String removeLastChar(String str) {
        return str.substring(0,str.length()-1);
    }
	
}
